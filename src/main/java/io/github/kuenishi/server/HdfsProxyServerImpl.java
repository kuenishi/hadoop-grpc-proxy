package io.github.kuenishi.server;

import io.github.kuenishi.server.generated.*;
import io.grpc.stub.StreamObserver;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class HdfsProxyServerImpl extends HdfsProxyGrpc.HdfsProxyImplBase {
    private static Logger LOG = LoggerFactory.getLogger(HdfsProxyServerImpl.class);
    private ConcurrentHashMap<Integer, HdfsFileHandleHolder> handles;
    private AtomicInteger counter;
    private FileSystem fs;

    public HdfsProxyServerImpl () throws IOException {
        handles = new ConcurrentHashMap<>();
        counter = new AtomicInteger(3);
        fs = FileSystem.get(new Configuration());
    }

    @Override
    public void open(OpenRequest req, StreamObserver<OpenResponse> responseObserver) {
        LOG.info("open({})", req.getPath());
        try {
            int fd = -1;
            FSDataInputStream in = fs.open(new Path(req.getPath()));
            synchronized (this) {
                fd = counter.getAndIncrement();
                assert !handles.containsKey(Integer.valueOf(fd));
                HdfsFileHandleHolder fileHandle = new HdfsFileHandleHolder(fd, in);
                handles.put(fd, fileHandle);
            }
            OpenResponse reply = OpenResponse.newBuilder().setFd(fd).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        } catch (IOException e) {
            LOG.warn("open failed", e);
            responseObserver.onError(e);
        }
    }

    @Override
    public void close(CloseRequest request, StreamObserver<CloseResponse> responseObserver) {
        try {
            synchronized (this) {
                HdfsFileHandleHolder handle = handles.remove(request.getFd());
                handle.close();
            }
            responseObserver.onNext(CloseResponse.newBuilder().build());
            responseObserver.onCompleted();
        } catch (IOException e) {
            LOG.warn("Close might have failed:", e);
            responseObserver.onError(e);
        }
    }

    @Override
    public void create(CreateRequest request, StreamObserver<CreateResponse> responseObserver) {
        LOG.info("create({})", request.getPath());
        try {
            int fd = -1;
            FSDataOutputStream out = fs.create(new Path(request.getPath()));
            synchronized (this) {
                fd = counter.getAndIncrement();
                assert !handles.containsKey(Integer.valueOf(fd));
                HdfsFileHandleHolder fileHandle = new HdfsFileHandleHolder(fd, out);
                handles.put(fd, fileHandle);
            }
            CreateResponse reply = CreateResponse.newBuilder().setFd(fd).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        } catch (IOException e) {
            LOG.warn("create failed", e);
            responseObserver.onError(e);
        }
    }

    @Override
    public void read(ReadRequest request, StreamObserver<ReadResponse> responseObserver) {
        super.read(request, responseObserver);
    }

    @Override
    public StreamObserver<WriteRequest> write(StreamObserver<WriteResponse> responseObserver) {
        return super.write(responseObserver);
    }
}
