package io.github.kuenishi.server;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;

import java.io.Closeable;
import java.io.IOException;
import java.util.Optional;

public class HdfsFileHandleHolder implements Closeable {
    private Optional<FSDataInputStream> in;
    private Optional<FSDataOutputStream> out;
    private int fd;
    // TODO: KeepAlive-like feature, by detecting inactive time?

    public HdfsFileHandleHolder(int fd, FSDataInputStream in) {
        this.fd = fd;
        this.in = Optional.of(in);
        this.out = Optional.empty();
    }
    public HdfsFileHandleHolder(int fd, FSDataOutputStream out) {
        this.fd = fd;
        this.in = Optional.empty();
        this.out = Optional.of(out);
    }

    public int getFd() {
        return fd;
    }

    @Override
    synchronized public void close() throws IOException {
        if (in.isPresent()) {
            in.get().close();
        }
        if (out.isPresent()) {
            out.get().close();
        }
    }
}
