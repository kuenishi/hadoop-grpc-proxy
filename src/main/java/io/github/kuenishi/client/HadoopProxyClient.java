package io.github.kuenishi.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.io.Closeable;
import java.util.concurrent.TimeUnit;
import io.github.kuenishi.server.generated.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// For internal test use
public class HadoopProxyClient implements Closeable {
    private static final Logger LOG = LoggerFactory.getLogger(HadoopProxyClient.class);

    private final ManagedChannel channel;
    private final HdfsProxyGrpc.HdfsProxyBlockingStub blockingStub;

    public HadoopProxyClient(String host, int port) {
        this(ManagedChannelBuilder.forAddress(host, port)
                // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                // needing certificates.
                .usePlaintext()
                .build());
    }

    HadoopProxyClient(ManagedChannel channel) {
        this.channel = channel;
        blockingStub = HdfsProxyGrpc.newBlockingStub(channel);
    }

    public void close() {
        try {
            channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOG.warn("Interrupted during close", e);
        }
    }

    public HdfsFileHandle open(String path) throws StatusRuntimeException {
        OpenRequest request = OpenRequest.newBuilder().setPath(path).build();
        OpenResponse response = blockingStub.open(request);
        return new HdfsFileHandle(response.getFd(), this);
    }

    public void closeFile(int fd) {
        CloseRequest request = CloseRequest.newBuilder().setFd(fd).build();
        blockingStub.close(request) ;
    }

}
