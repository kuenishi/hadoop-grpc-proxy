package io.github.kuenishi.client;

import java.io.Closeable;

public class HdfsFileHandle implements Closeable {

    private int fd;
    private HadoopProxyClient client;
    HdfsFileHandle(int fd, HadoopProxyClient client) {
        this.fd = fd;
        this.client = client;
    }
    public void close() {
        client.closeFile(fd);
    }
}
