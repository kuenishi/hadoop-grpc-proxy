package io.github.kuenishi.server;

import io.github.kuenishi.client.HadoopProxyClient;
import io.github.kuenishi.client.HdfsFileHandle;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class HadoopProxyClientTest {
    private HadoopProxyServer server;
    //private HadoopProxyClient client;

    @Before
    public void setup() throws IOException {
        server = new HadoopProxyServer();
        server.start();
    }

    @After
    public void teardown() {
        server.stop();
    }

    @Test
    public void testClient() throws IOException {
        try (HadoopProxyClient client = new HadoopProxyClient("localhost", 50051)) {
            HdfsFileHandle handle = client.open("/piath/me");
            //assertEquals(42, fd);
            handle.close();
        } catch (Throwable e) {
            e.printStackTrace(System.err);
        }
    }
}
